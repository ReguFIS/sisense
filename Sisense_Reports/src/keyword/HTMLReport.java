package keyword;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;


public class HTMLReport {

	static BufferedWriter bw1 = null;
	static BufferedWriter bw2 = null;
	static BufferedWriter bw3 = null;
	static BufferedWriter bw4 = null;
	static BufferedWriter bw5 = null;
	
	public void createResultsfile1 (String HTMLReportPath, String strTestcaseName) throws IOException {
		
		bw1 = new BufferedWriter(new FileWriter(HTMLReportPath));
		bw1.write ("<html>");
		bw1.newLine();
		bw1.write ("<table border = 1 width=1210 bgcolor='#b3ff66'>");
		bw1.newLine();
		bw1.write("<tr align='center'><td><font color= Blue>"+strTestcaseName +"</td></tr>");
		bw1.newLine();
		bw1.write ("</table>");
		bw1.newLine();
		bw1.write ("<table border = 1>");
		bw1.newLine();
		bw1.write ("<tr bgcolor = '#b3ff66'>");
		bw1.newLine();
		bw1.write ("<th width=70>Step_ID</th>");
		bw1.newLine();
		bw1.write ("<th  width=200>Action To Perform</th>");
		bw1.newLine();
		bw1.write ("<th  width=300>Test Data</th>");
		bw1.newLine();
		bw1.write ("<th  width=200>Expected Result</th>");
		bw1.newLine();
		bw1.write ("<th  width=200>Actual Result</th>");
		bw1.newLine();
		bw1.write ("<th  width=200>Status</th></tr>");
		bw1.newLine();
		bw1.write ("</table>");
		bw1.newLine();
		bw1.write ("</html>");
		bw1.close();
		
	}
	
	
	public void create_TCResults1 (String HTMLReportPath, String stepno, String action_perform, String testdata, String expected, String actual, String status ) throws IOException {
		
			
		 bw2 = new BufferedWriter(new FileWriter(HTMLReportPath, true));
		 bw2.newLine();
		 bw2.write ("<html>");
		 bw2.newLine();
		 bw2.write ("<table border = 1>");
		 bw2.newLine();
		 bw2.write ("<tr bgcolor = '#e1e1d0'>");
		 bw2.newLine();
		 bw2.write ("<td width=70>"+stepno+"</td>");
		 bw2.newLine();
		 bw2.write ("<td  width=200>"+action_perform+"</td>");
		 bw2.newLine();
		 bw2.write ("<td  width=300>"+testdata+"</td>");
		 bw2.newLine();
		 bw2.write ("<td  width=200>"+expected+"</td>");
		 bw2.newLine();
			if(actual.endsWith("Not Found")) {
				 bw2.write ("<td  width=200><font color='RED'>"+actual+"</td>");	}
			else {
				 bw2.write ("<td  width=200>"+actual+"</td>");	}
		 
		 	if (status.equals("Pass")) {
		 		bw2.newLine();
		 		bw2.write ("<td width=200 align='center'><font color='GREEN'>"+status+"</font></td>");}
		 		else	{
		 		bw2.newLine();
		 		bw2.write ("<td width=200 align='center'><font color='RED'>"+status+"</font></td>");
		 		}
		 bw2.newLine();
		 bw2.write ("</tr>");
		 bw2.newLine();
		 bw2.write ("</table>");
		 bw2.newLine();
		 bw2.write ("</html>");
		 bw2.close();
	}
	
	public void TestExecutionReportTemplate (String systempath, String Heading) throws IOException {
		
		bw3 = new BufferedWriter(new FileWriter(systempath));
		bw3.newLine();
		bw3.write ("<html>");
		bw3.newLine();
		bw3.write ("<table border = 1 width=922>");
		bw3.newLine();
		bw3.write("<tr height = 75><td bgcolor='#75923C' align='center' style='font-size:25.0pt'> " + Heading + " </td></tr>");
		bw3.newLine();
		bw3.write ("</table>");
		bw3.newLine();
		bw3.write ("<br>");
		bw3.write ("<table border = 1 width=922>");
		bw3.newLine();
		bw3.write("<tr><td bgcolor='#75923C' align='center' style='font-size:20.0pt'> Detailed Test Report</td></tr>");
		bw3.newLine();
		bw3.write ("</table>");
		bw3.newLine();
		bw3.write ("<table border = 1>");
		bw3.newLine();
		bw3.write ("<tr bgcolor = '#75923C'>");
		bw3.newLine();
		bw3.write ("<th width=350>Test Case Name</th>");
		bw3.newLine();
		bw3.write ("<th  width=200>Status</th>");
		bw3.newLine();
		bw3.write ("<th  width=350>Details</th>");
		bw3.newLine();
		bw3.write ("</tr>");
		bw3.newLine();
		bw3.write ("</table>");
		bw3.newLine();
		bw3.write ("</html>");
		bw3.close();
	}

	public void TestExecutionReportUpdate (String systempath, String HTMLReportPath, String TestCaseName, String Status, String Details) throws IOException {
		
		bw4 = new BufferedWriter(new FileWriter(systempath, true));
		bw4.newLine();
		bw4.write ("<html>");
		bw4.newLine();
		bw4.write ("<table border = 1 >");
		bw4.newLine();
		bw4.write ("<tr align='center'>");
		bw4.newLine();
		if (!HTMLReportPath.isEmpty()){
			bw4.write ("<td width=350 align='left'><a href = "+HTMLReportPath+">"+TestCaseName+" </a></td>");
			bw4.newLine();		}
		else {
			bw4.write ("<td width=350 align='left'>"+TestCaseName+" </a></td>");
			bw4.newLine();		}
			
		if (Status.equalsIgnoreCase("Pass")){
			bw4.write ("<td width=200 align='left'><font color='GREEN'>"+Status+"</td>");
			bw4.newLine();	}
		else if (Status.equalsIgnoreCase("Fail")){
			bw4.write ("<td width=200 align='left'><font color='RED'>"+Status+"</td>");
			bw4.newLine();	}	
		else {
			bw4.write ("<td width=200 align='left'><font color='BLUE'>"+Status+"</td>");
			bw4.newLine();	}
		
		bw4.write ("<td  width=350 align='left'>"+Details+"</td>");
		bw4.newLine();
		bw4.write ("</tr>");
		bw4.newLine();
		bw4.write ("</table>");
		bw4.newLine();
		bw4.write ("</html>");
		bw4.close();
	}

	
	public void TestExecutionReportStatusUpdate (String systempath, String Project_Name, Date Start_Time, Date End_Time, String HostName, String OSName, int TC_Passed, int TC_Failed, int  NOT_EXECUTED) throws IOException	{
		
		bw5 = new BufferedWriter(new FileWriter(systempath, true));
		bw5.write ("<html>");
		bw5.write ("<br>");
		bw5.write ("<table border = 1 width=922 >");
		bw5.write ("<tr bgcolor = '#75923C'>");
		bw5.write ("<th colspan='2'>Environmental Details</th>");
		bw5.write ("</tr>");
		bw5.write ("<tr >");
		bw5.write ("<td width = 50%> Test Name: "+Project_Name+" </td>");
		bw5.write ("<td width = 50%> Testing Tool: Selenium Webdriver </td>");
		bw5.write ("</tr>");
		bw5.write ("<tr>");
		bw5.write ("<td width = 50%> Start Time: "+Start_Time+" </td>");
		bw5.write ("<td width = 50%> Host Name: "+HostName+"</td>");
		bw5.write ("</tr>");
		bw5.write ("<tr>");
		bw5.write ("<td width = 50%> End Time: "+End_Time+"</td>");
		bw5.write ("<td width = 50%> OS: "+OSName+"</td>");
		bw5.write ("</tr>");
		bw5.write ("</table>");
		bw5.write ("<br>");
		bw5.write ("<table border = 1 width = 922>");
		bw5.write ("<tr bgcolor = '#75923C'>");
		bw5.write ("<th width = 50%> STATUS </th>");
		bw5.write ("<th width = 50%> COUNT </th>");
		bw5.write ("</tr>");
		bw5.write ("<tr>");
		bw5.write ("<td width = 50%><font color='GREEN'> PASSED </td>");
		bw5.write ("<td width = 50%><font color='GREEN'> "+TC_Passed+" </td>");
		bw5.write ("</tr>");
		bw5.write ("<tr>");
		bw5.write ("<td width = 50%><font color='RED'> FAILED </td>");
		bw5.write ("<td width = 50% style = 'border: 1px solid #FF0000'><font color='RED'> "+TC_Failed+" </td>");
		bw5.write ("</tr>");
		bw5.write ("<tr>");
		bw5.write ("<td width = 50%> NOT EXECUTED </td>");
		bw5.write ("<td width = 50%> "+NOT_EXECUTED+" </td>");
		bw5.write ("</tr>");
		bw5.write ("</table>");
		bw5.write ("</html>");
		bw5.close();
		}
	
	
}