package keyword;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;


public class ReadExcel {

	XSSFWorkbook wbWorkbook;
	XSSFSheet shSheet;
	
	public void openSheet(String filePath) {
		FileInputStream fs;
		try {
			fs = new FileInputStream(filePath);
			wbWorkbook = new XSSFWorkbook(fs);
			shSheet = wbWorkbook.getSheet("TC_Sheet");

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void openTCSheet(String filePath, String TCName) {
		FileInputStream fs;
		try {
			fs = new FileInputStream(filePath);
			wbWorkbook = new XSSFWorkbook(fs);
			shSheet = wbWorkbook.getSheet(TCName);

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}  catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public String getValueFromCell(int iRowNumber, int iColNumber) {
		return shSheet.getRow(iRowNumber).getCell(iColNumber).getStringCellValue();
				
	}

	public int getRowCount() {
		return shSheet.getPhysicalNumberOfRows();
	}

	public int getColumnCount(int iRowNumber) {
		return shSheet.getRow(iRowNumber).getPhysicalNumberOfCells();
	}
}